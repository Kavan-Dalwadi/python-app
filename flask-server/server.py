from flask import Flask

app = Flask(__name__)

@app.route("/interns")
def interns():
    return {"interns": ["kavan","om"]}

if __name__ == "__main__":
    app.run(debug=True)
